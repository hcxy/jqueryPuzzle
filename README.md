##jqueryPuzzle
jqueryPuzzle是适应手机版的手机拼图游戏。修改自我在网上看到的一个效果[http://www.17sucai.com/pins/demoshow/5028]

##如何使用

* 在页面内引入jquery和puzzle.js
* `var pg = new puzzleGame({'img':'http://ijifu-site.qiniudn.com/active_yuandan15.jpg', 'imgInfo' : {'width':630, 'height':840}, 'statbtn' : '#start', 'imgArea' : '#imgArea', 'btnLevel' : '#wrap #left ul #level span'})`
参数说明：
* `pg.beforeStart = function(){
	//开始的判断
	return true;
}`
* `pg.successed = function(costTime){
	//拼图成功调用的方法costTime为消耗的时间（秒）
        console.log(costTime);
    }`
* 当需要还原的时候需调用下面这个方法

##为什么修改的项目也放上来
* 在网上查阅一些资料的时候发现手机端的比较少
* 源码中对于实际应用情况考虑的比较少
* 记录一下证明自己学习过

##有问题反馈
在使用中有任何问题，欢迎反馈给我，可以用以下联系方式跟我交流

* QQ: 893157632